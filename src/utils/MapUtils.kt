package com.code.analyzer.utils

import java.util.*
import kotlin.collections.LinkedHashMap
import kotlin.collections.Map.Entry

fun <K, V : Comparable<V>> sortByValueDesc(map: Map<K, V>): Map<K, V> {
	val list: ArrayList<Entry<K, V>> = ArrayList(map.entries)
	list.sortByDescending { it.value }
	val result: MutableMap<K, V> = LinkedHashMap()
	for ((key, value) in list) {
		result[key] = value
	}
	return result
}

fun <K, V : Comparable<V>> sortByListSizeDesc(map: Map<K, List<V>>): Map<K, List<V>> {
	val list: ArrayList<Entry<K, List<V>>> = ArrayList(map.entries)
	list.sortByDescending { it.value.size }
	val result: MutableMap<K, List<V>> = LinkedHashMap()
	for ((key, value) in list) {
		result[key] = value
	}
	return result
}