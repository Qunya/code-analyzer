package dao

import com.mchange.v2.c3p0.ComboPooledDataSource
import dao.management.DAOFacade
import dao.management.DAOFacadeDatabase
import io.ktor.application.Application
import io.ktor.application.ApplicationStopped
import io.ktor.util.KtorExperimentalAPI
import org.jetbrains.exposed.sql.Database
import java.io.File

var dao: DAOFacade? = null
val dir = File("build/db")

@KtorExperimentalAPI
@Suppress("unused")
fun Application.getPool(): ComboPooledDataSource {
    return ComboPooledDataSource().apply {
        driverClass = org.h2.Driver::class.java.name
        jdbcUrl = "jdbc:h2:file:${dir.canonicalFile.absolutePath}"
    }
}

@KtorExperimentalAPI
fun Application.dao() {
    val ds = getPool()
    dao = DAOFacadeDatabase(Database.connect(ds))
    environment.monitor.subscribe(ApplicationStopped) { ds.connection.close() }
}