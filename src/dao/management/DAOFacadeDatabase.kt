package dao.management

import com.code.analyzer.dao.models.File
import com.code.analyzer.dao.models.History
import com.code.analyzer.dao.models.PotentialBug
import com.code.analyzer.dao.models.Project
import com.code.analyzer.dao.models.User
import com.code.analyzer.dao.tables.Files
import com.code.analyzer.dao.tables.Histories
import com.code.analyzer.dao.tables.PotentialBugs
import com.code.analyzer.dao.tables.Projects
import dao.tables.Users
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import java.io.*

interface DAOFacade : Closeable {

	fun user(nickname: String, hash: String? = null): User?

	fun findUser(nickname: String, hash: String? = null): Boolean

	fun findUserProjects(owner: Long): ArrayList<Project>

	fun createUser(user: User)

	fun createProject(userId: Long, project: Project)

	fun findProject(userId: Long, projectName: String): Project?

	fun findProject(projectId: Long): Project?

	fun removeProject(userId: Long, name: String)

	fun createHistory(history: History)

	fun createFile(file: File)

	fun createBug(bug: PotentialBug)

	fun findBug(fileId: Long): PotentialBug?

	fun findProjectFiles(projectId: Long, time: DateTime): ArrayList<File>

	fun updateProject(project: Project)

	fun getAllProjectHistory(projectId: Long): ArrayList<History>
}

class DAOFacadeDatabase(val db: Database) : DAOFacade {

	init {
		transaction {
			SchemaUtils.create(Users, Projects, Histories, Files, PotentialBugs)
		}
	}

	override fun user(nickname: String, hash: String?) = transaction {
		Users.select { Users.nickname.eq(nickname) }
			.mapNotNull {
				if (hash == null || it[Users.passwordHash] == hash) {
					it[Users.email]?.let { it1 -> User(it[Users.id], nickname, it1, it[Users.passwordHash]) }
				} else {
					null
				}
			}
			.singleOrNull()
	}

	override fun findUser(nickname: String, hash: String?) = user(nickname) != null

	override fun createUser(user: User) = transaction {
		Users.insert {
			it[nickname] = user.nickname
			it[email] = user.email
			it[passwordHash] = user.passwordHash
		}
		Unit
	}

	override fun findUserProjects(owner: Long): ArrayList<Project> = transaction {
		val projects = arrayListOf<Project>()
		Projects.select { Projects.owner.eq(owner) }.mapNotNull {
			projects.add(
				Project(
					it[Projects.id], it[Projects.owner], it[Projects.name], it[Projects.status],
					it[Projects.branch], it[Projects.version], it[Projects.bugs], it[Projects.lang],
					it[Projects.coverage], it[Projects.newCoverage], it[Projects.lines], it[Projects.changeClassPercent]
				)
			)
		}
		projects
	}

	override fun createProject(userId: Long, project: Project) = transaction {
		Projects.insert {
			it[owner] = userId
			it[name] = project.name
			it[status] = project.status
		}
		Unit
	}

	override fun findProject(userId: Long, projectName: String): Project? = transaction {
		Projects.select { Projects.owner.eq(userId).and(Projects.name.eq(projectName)) }
			.mapNotNull {
				Project(
					it[Projects.id], it[Projects.owner], it[Projects.name], it[Projects.status],
					it[Projects.branch], it[Projects.version], it[Projects.bugs], it[Projects.lang],
					it[Projects.coverage], it[Projects.newCoverage], it[Projects.lines], it[Projects.changeClassPercent]
				)
			}.singleOrNull()
	}

	override fun findProject(projectId: Long): Project? = transaction {
		Projects.select { Projects.id.eq(projectId) }
			.mapNotNull {
				Project(
					it[Projects.id], it[Projects.owner], it[Projects.name], it[Projects.status],
					it[Projects.branch], it[Projects.version], it[Projects.bugs], it[Projects.lang],
					it[Projects.coverage], it[Projects.newCoverage], it[Projects.lines], it[Projects.changeClassPercent]
				)
			}.singleOrNull()
	}

	override fun removeProject(userId: Long, name: String) = transaction {
		Projects.deleteWhere { Projects.owner.eq(userId).and(Projects.name.eq(name)) }
		Unit
	}

	override fun updateProject(project: Project) = transaction {
		Projects.update({ Projects.id.eq(project.id) }) {
			it[status] = project.status
			it[branch] = project.branch
			it[version] = project.version
			it[bugs] = project.bugs
			it[lang] = project.lang
			it[coverage] = project.coverage
			it[newCoverage] = project.newCoverage
			it[lines] = project.lines
			it[changeClassPercent] = project.classChangePercent
		}
		Unit
	}

	override fun createHistory(history: History) = transaction {
		Histories.insert {
			it[projectId] = history.projectId
			it[time] = history.time
			it[action] = history.action
		}
		Unit
	}

	override fun getAllProjectHistory(projectId: Long): ArrayList<History> = transaction {
		val history = arrayListOf<History>()
		Histories.select { Histories.projectId.eq(projectId) }.mapNotNull {
			history.add(History(it[Histories.id], it[Histories.projectId], it[Histories.time], it[Histories.action]))
		}
		history
	}

	override fun createFile(file: File) = transaction {
		Files.insert {
			it[name] = file.name
			it[projectId] = file.projectId
			it[lines] = file.lines
			it[changes] = file.changes!!
			it[bugs] = file.bugs!!
			it[coverage] = file.coverage
			it[time] = file.time!!
			it[topByCommit] = file.topEdit
			it[topByBugs] = file.topProtect
		}
		Unit
	}


	override fun createBug(bug: PotentialBug) = transaction {
		PotentialBugs.insert {
			it[fileId] = bug.fileId
			it[changes] = bug.changes
			it[newChanges] = bug.newChanges
			it[bugs] = bug.bugs
			it[newBugs] = bug.NewBugs
			it[coverage] = bug.coverage
			it[newCoverage] = bug.newCoverage
		}
		Unit
	}

	override fun findBug(fileId: Long): PotentialBug? = transaction {
		PotentialBugs.select { PotentialBugs.fileId.eq(fileId) }.mapNotNull {
			PotentialBug(
				it[PotentialBugs.id], fileId,
				it[PotentialBugs.changes], it[PotentialBugs.newChanges], it[PotentialBugs.bugs],
				it[PotentialBugs.newBugs], it[PotentialBugs.coverage], it[PotentialBugs.newCoverage]
			)
		}.singleOrNull()
	}

	override fun findProjectFiles(projectId: Long, time: DateTime) = transaction {
		val array = arrayListOf<File>()
		Files.select { Files.projectId.eq(projectId).and(Files.time.eq(time)) }.mapNotNull {
			array.add(File(it[Files.id], it[Files.name], it[Files.projectId], it[Files.lines], it[Files.changes], it[Files.bugs], it[Files.time], it[Files.coverage],
			it[Files.topByCommit], it[Files.topByBugs]))
		}
		array
	}

	override fun close() {
	}
}