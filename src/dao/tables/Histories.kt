package com.code.analyzer.dao.tables

import org.jetbrains.exposed.sql.Table

object Histories : Table() {
	val id = long("historyId").primaryKey().autoIncrement()
	val projectId = long("projectId")
	val time = date("time")
	val action = integer("action")
}