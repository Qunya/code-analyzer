package dao.tables

import org.jetbrains.exposed.sql.*

object Users : Table() {
    val id = long("id").primaryKey().autoIncrement()
    val nickname = varchar("nickname", 20)
    val email = varchar("email", 128).nullable()
    val passwordHash = varchar("password_hash", 128)
}