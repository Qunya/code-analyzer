package com.code.analyzer.dao.tables

import org.jetbrains.exposed.sql.Table

object Files : Table() {
	val id = long("fileId").primaryKey().autoIncrement()
	val name = varchar("name", 150)
	val projectId = long("projectId")
	val lines = integer("lines")
	val changes = integer("changes")
	val bugs = integer("bugs")
	val coverage = double("coverage")
	val time = date("time")
	val topByCommit = integer("topEdit")
	val topByBugs = integer("topBugs")
}