package com.code.analyzer.dao.tables

import org.jetbrains.exposed.sql.Table

object PotentialBugs : Table() {
	val id = long("bugId").primaryKey().autoIncrement()
	val fileId = long("fileId")
	val changes = integer("changes")
	val bugs = integer("bugs")
	val newChanges = integer("newChanges")
	val newBugs = integer("newBugs")
	val coverage = integer("coverage")
	val newCoverage = integer("newCoverage")
}