package com.code.analyzer.dao.tables

import org.jetbrains.exposed.sql.Table

object Projects : Table() {
	val id = long("projectId").primaryKey().autoIncrement()
	val owner = long("owner")
	val name = varchar("name", 50).uniqueIndex()
	val status = integer("status")
	val branch = varchar("branch", 50).nullable()
	val version = double("version").nullable()
	val bugs = integer("bugs").nullable()
	val coverage = double("coverage").nullable()
	val newCoverage = double("newCoverage").nullable()
	val lang = varchar("lang", 20).nullable()
	val lines = integer("lines").nullable()
	val changeClassPercent = integer("changeClassPercent").nullable()
}