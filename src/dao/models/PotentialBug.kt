package com.code.analyzer.dao.models

import java.io.Serializable

data class PotentialBug(val id: Long, val fileId: Long, val changes: Int, val newChanges: Int, val bugs: Int, val NewBugs: Int, val coverage: Int, val newCoverage: Int) : Serializable