package com.code.analyzer.dao.models

import java.io.Serializable

data class Project(val id: Long, val owner: Long, val name: String, var status: Int) : Serializable {
	constructor(
		id: Long, owner: Long, name: String, status: Int, branch: String?,
		version: Double?, bugs: Int?, lang: String?, coverage: Double?,
		newCoverage: Double?, lines: Int?, classChangePercent: Int?
	) : this(id, owner, name, status) {
		this.branch = branch
		this.version = version
		this.bugs = bugs
		this.lang = lang
		this.coverage = coverage
		this.newCoverage = newCoverage
		this.lines = lines
		this.classChangePercent = classChangePercent
	}

	var branch: String? = null
	var version: Double? = null
	var bugs: Int? = null
	var lang: String? = null
	var coverage: Double? = null
	var newCoverage: Double? = null
	var lines: Int? = null
	var classChangePercent: Int? = null
}