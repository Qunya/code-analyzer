package com.code.analyzer.dao.models

import org.joda.time.DateTime
import java.io.Serializable

data class File(val id: Long, val name: String, val projectId: Long, val lines: Int, val changes: Int?, val bugs: Int?,
                val time: DateTime?, val coverage: Double, val topEdit: Int, val topProtect: Int) : Serializable
