package com.code.analyzer.dao.models

import org.joda.time.DateTime

data class History(val id: Long, val projectId: Long, val time: DateTime, val action: Int)