package com.code.analyzer.dao.models

import java.io.Serializable

data class User(val id: Long, val nickname: String, val email: String, val passwordHash: String) : Serializable
data class UserInfo(val nickname: String, val email: String) : Serializable