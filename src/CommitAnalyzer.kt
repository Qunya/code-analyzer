package com.code.analyzer

import com.code.analyzer.utils.sortByListSizeDesc
import com.code.analyzer.utils.sortByValueDesc
import com.google.common.collect.Maps
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.errors.GitAPIException
import org.eclipse.jgit.diff.DiffEntry
import org.eclipse.jgit.diff.DiffEntry.ChangeType.ADD
import org.eclipse.jgit.diff.DiffEntry.ChangeType.COPY
import org.eclipse.jgit.diff.DiffEntry.ChangeType.DELETE
import org.eclipse.jgit.diff.DiffEntry.ChangeType.MODIFY
import org.eclipse.jgit.diff.DiffEntry.ChangeType.RENAME
import org.eclipse.jgit.diff.DiffFormatter
import org.eclipse.jgit.diff.RawTextComparator
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.revwalk.RevCommit
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.util.io.DisabledOutputStream
import java.io.File
import java.io.IOException
import java.util.stream.Collectors
import java.util.stream.StreamSupport

private val BUG_DETECT_WORD = listOf("fix", "bug")
private val EXTENSIONS = listOf(".java", ".kt", ".js")

class CommitAnalyzer
	(
	private val git: Git,
	private val files: MutableMap<String, Int>,
	private val bugs: MutableMap<String, MutableList<String>>
) {
	@Throws(Exception::class)
	fun analyze(): Pair<Map<String, Int>, Map<String, List<String>>> {
		val repository = git.repository
		val walk = RevWalk(repository)
		val diffFormatter = buildDiffFormatter(repository)
		val commits: List<RevCommit?> = try {
			retrieveCommits()
		} catch (e: GitAPIException) {
			throw Exception("Error occurred retrieving commits")
		}
		val analyzedFiles = analyzeCommits(commits, walk, diffFormatter)
		return Pair(sortByValueDesc(analyzedFiles), sortByListSizeDesc(bugs))
	}

	@Throws(Exception::class)
	private fun analyzeCommits(
		commits: List<RevCommit?>,
		walk: RevWalk,
		diffFormatter: DiffFormatter
	): Map<String, Int> {
		for (commit in commits) {
			if (commit != null && isCheckableCommit(commit)) {
				val parent: RevCommit = try {
					walk.parseCommit(commit.parents[0].id)
				} catch (e: IOException) {
					throw Exception("Error occurred parsing parent of commit")
				}
				val diffs = try {
					diffFormatter.scan(parent.tree, commit.tree)
				} catch (e: IOException) {
					throw Exception("Error occurred scanning diffs")
				}
				for (diff in diffs) {
					when (isBugfix(commit.fullMessage)) {
						true -> diffProcessing(diff, commit.name)
						false -> diffProcessing(diff)
					}
				}
			}
		}
		return files
	}

	@Throws(GitAPIException::class)
	private fun retrieveCommits(): List<RevCommit?> = StreamSupport.stream(git.log().call().spliterator(), false).collect(Collectors.toList()).reversed()

	private fun buildDiffFormatter(repository: Repository): DiffFormatter {
		val diffFormatter = DiffFormatter(DisabledOutputStream.INSTANCE)
		diffFormatter.setRepository(repository)
		diffFormatter.setDiffComparator(RawTextComparator.DEFAULT)
		diffFormatter.isDetectRenames = true
		return diffFormatter
	}

	private fun diffProcessing(
		diff: DiffEntry
	) {
		if (isJavaOrKotlinFile(diff)) {
			val newPath = diff.newPath
			val oldPath = diff.oldPath
			when (diff.changeType!!) {
				DELETE -> if (bugs.containsKey(oldPath)) {
					files.remove(oldPath)
					bugs.remove(oldPath)
				} else {
					files.remove(oldPath)
				}
				ADD, MODIFY -> if (files.containsKey(newPath)) {
					files.replace(newPath, files[newPath]!! + 1)
				} else {
					files[newPath] = 1
				}
				COPY, RENAME -> if (files.containsKey(oldPath)) {
					files[newPath] = files[oldPath]!! + 1
					files.remove(oldPath)
				} else {
					files[newPath] = 1
				}
			}
		}
	}

	private fun diffProcessing(
		diff: DiffEntry,
		commitName: String
	) {
		if (isJavaOrKotlinFile(diff)) {
			val newPath = diff.newPath
			val oldPath = diff.oldPath
			when (diff.changeType!!) {
				DELETE -> if (bugs.containsKey(oldPath)) {
					files.remove(oldPath)
					bugs.remove(oldPath)
				} else {
					files.remove(oldPath)
				}
				ADD, MODIFY -> if (files.containsKey(newPath)) {
					files.replace(newPath, files[newPath]!! + 1)
					if (bugs.containsKey(newPath)) {
						val bugList = bugs[newPath]
						bugList!!.add(commitName)
						bugs.replace(newPath, bugList)
					} else {
						bugs[newPath] = mutableListOf(commitName)
					}
				} else {
					files[newPath] = 1
					bugs[newPath] = mutableListOf(commitName)
				}
				COPY, RENAME -> if (files.containsKey(oldPath)) {
					files[newPath] = files[oldPath]!! + 1
					files.remove(oldPath)
					if (bugs.containsKey(oldPath)) {
						val bugList = bugs[oldPath]
						bugList!!.add(commitName)
						bugs[newPath] = bugList
						bugs.remove(oldPath)
					} else {
						bugs[newPath] = mutableListOf(commitName)
					}
				} else {
					files[newPath] = 1
					bugs[newPath] = mutableListOf(commitName)
				}
			}
		}
	}

	private fun isCheckableCommit(commit: RevCommit): Boolean {
		return commit.name.isNotBlank() && commit.parents.isNotEmpty()
	}

	private fun isJavaOrKotlinFile(diff: DiffEntry): Boolean {
		return endsWith(diff, EXTENSIONS) && !(diff.oldPath.contains("test") || diff.newPath.contains("test") && (diff.oldPath.contains("src") || diff.newPath.contains("src")
				&& !(diff.oldPath.contains("build\\classes") || diff.newPath.contains("build\\classes"))))
	}

	private fun isBugfix(message: String): Boolean {
		return contains(message, BUG_DETECT_WORD)
	}

	private fun contains(line: String, list: List<String>): Boolean {
		val iter = list.listIterator()
		while (iter.hasNext()) {
			if (line.contains(iter.next())) {
				return true
			}
		}
		return false
	}

	private fun endsWith(diff: DiffEntry, list: List<String>): Boolean {
		val iter = list.listIterator()
		while (iter.hasNext()) {
			val extension = iter.next()
			if (diff.newPath.endsWith(extension) || diff.oldPath.endsWith(extension)) {
				return true
			}
		}
		return false
	}
}