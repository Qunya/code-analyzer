import com.code.analyzer.git.git
import dao.dao
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.authenticate
import io.ktor.auth.basic
import io.ktor.auth.principal
import io.ktor.features.AutoHeadResponse
import io.ktor.features.CachingHeaders
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.gson.gson
import io.ktor.http.CacheControl
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.CachingOptions
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Locations
import io.ktor.request.path
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.date.GMTDate
import org.slf4j.event.Level
import service.analyze

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module() {
	install(Locations) {
	}

	install(AutoHeadResponse)

	install(CallLogging) {
		level = Level.INFO
		filter { call -> call.request.path().startsWith("/") }
	}

	install(CachingHeaders) {
		options { outgoingContent ->
			when (outgoingContent.contentType?.withoutParameters()) {
				ContentType.Text.CSS -> CachingOptions(
					CacheControl.MaxAge(maxAgeSeconds = 24 * 60 * 60),
					expires = null as? GMTDate?
				)
				else -> null
			}
		}
	}

	install(Authentication) {
		basic("myBasicAuth") {
			realm = "Ktor Server"
			validate { if (it.name == "test" && it.password == "password") UserIdPrincipal(it.name) else null }
		}
	}

	install(ContentNegotiation) {
		gson {
		}
	}

	dao()

	routing {
		git()
		analyze()
		// Static feature. Try to access `/static/ktor_logo.svg`
		static("/static") {
			resources("static")
		}

		install(StatusPages) {
			exception<AuthenticationException> { cause ->
				call.respond(HttpStatusCode.Unauthorized)
			}
			exception<AuthorizationException> { cause ->
				call.respond(HttpStatusCode.Forbidden)
			}

		}

		authenticate("myBasicAuth") {
			get("/protected/route/basic") {
				val principal = call.principal<UserIdPrincipal>()!!
				call.respondText("Hello ${principal.name}")
			}
		}
	}
}

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()