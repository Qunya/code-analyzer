package com.code.analyzer.git

import com.code.analyzer.dao.models.History
import com.code.analyzer.dao.models.Project
import com.code.analyzer.git.responses.ProjectInfoResponse
import com.google.gson.Gson
import dao.dao
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.locations.post
import io.ktor.request.receive
import io.ktor.response.respondText
import io.ktor.routing.Route
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.eclipse.jgit.api.Git
import org.joda.time.DateTime
import service.models.Action
import service.models.Action.START_ANALYZED
import service.models.getAction
import service.models.getMessage
import service.responses.HistoryResponse
import service.responses.ProjectsResponse
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

private const val GIT_EXTENSION = ".git"
private const val SLASH = "/"
val GSON = Gson()

@KtorExperimentalLocationsAPI
@Location("/add")
data class GitAdd(val userId: Long = 0, val repositoryUrl: String = "", val repositoryName: String = "")

@KtorExperimentalLocationsAPI
@Location("/info")
data class ProjectInfo(val userId: Long = 0, val repositoryName: String = "")

@KtorExperimentalLocationsAPI
@Location("/projects")
data class Projects(val userId: Long = 0)

@KtorExperimentalLocationsAPI
@Location("/history")
data class HistoryPost(val projectId: Long = 0)

@KtorExperimentalLocationsAPI
fun Route.git() {
	post<GitAdd> {
		val request = call.receive<GitAdd>()
		val project = Project(0, request.userId, request.repositoryName, 1)
		dao!!.createProject(request.userId, project)
		dao!!.findProject(request.userId, request.repositoryName)?.let { dao!!.createHistory(History(0, it.id, DateTime(System.currentTimeMillis()), Action.CREATED.id)) }
		try {
			withContext(Dispatchers.IO) {
				val dir = File(request.userId.toString() + SLASH + request.repositoryName)
				if (!dir.exists()) {
					dir.mkdir()
					cloneRepository(request.repositoryUrl, dir)
				} else {
					call.respondText { "Already exist" }
				}
			}
		} catch (e: IOException) {
			dao!!.removeProject(request.userId, request.repositoryName)
		}
	}

	post<ProjectInfo> {
		val request = call.receive<ProjectInfo>()
		try {
			val git = withContext(Dispatchers.IO) { Git.open(File(request.userId.toString() + SLASH + request.repositoryName + SLASH + GIT_EXTENSION)) }
			call.respondText(
				GSON.toJson(ProjectInfoResponse(git.repository.branch, 1.0, null, null), ProjectInfoResponse::class.java), ContentType.Application.Json, HttpStatusCode.OK
			)
		} catch (e: IOException) {

		}
	}

	post<Projects> {
		val request = call.receive<Projects>()
		try {
			val response = arrayListOf<ProjectsResponse.Project>()
			dao!!.findUserProjects(request.userId).forEach {
				response.add(fillProjectResponse(it))
			}
			call.respondText(
				GSON.toJson(ProjectsResponse(response), ProjectsResponse::class.java), ContentType.Application.Json, HttpStatusCode.OK
			)
		} catch (e: IOException) {

		}
	}

	post<HistoryPost> {
		val request = call.receive<HistoryPost>()
		val response = arrayListOf<HistoryResponse.History>()
		val formatter = SimpleDateFormat("dd.MM.yyyy HH:mm")
		dao!!.getAllProjectHistory(request.projectId).forEach {
			response.add(HistoryResponse.History(getAction(it.action), formatter.format(it.time.toDate())))
		}
		call.respondText(
			GSON.toJson(HistoryResponse(response), HistoryResponse::class.java), ContentType.Application.Json, HttpStatusCode.OK
		)
	}
}

suspend fun updateProject(userId: Long, repositoryName: String, projectId: Long, dateTime: DateTime) {
	withContext(Dispatchers.IO) { Git.open(File(userId.toString() + SLASH + repositoryName)).pull() }
	dao!!.createHistory(History(0, projectId, dateTime, START_ANALYZED.id))
}

@KtorExperimentalLocationsAPI
fun cloneRepository(repositoryUrl: String, dir: File) {
	Git.cloneRepository().setURI(repositoryUrl).setDirectory(dir).call()
}

fun fillProjectResponse(project: Project): ProjectsResponse.Project {
	return if (project.status > 0) {
		val (change, update) = getLastUpdateChangeAction(dao!!.getAllProjectHistory(project.id))
		ProjectsResponse.Project(
			project.id.toString(),
			project.name,
			getMessage(project.status),
			project.lang,
			project.coverage,
			change,
			update,
			project.bugs,
			project.lines,
			project.classChangePercent
		)
	} else {
		ProjectsResponse.Project(project.id.toString(), project.name, getMessage(project.status))
	}
}

fun getLastUpdateChangeAction(histories: ArrayList<History>): Pair<Date?, Date?> {
	var change: Date? = null
	var update: Date? = null
	histories.forEach {
		if (update == null && it.action == 2) {
			update = it.time.toDate()
		} else if (change == null && it.action == 3) {
			change = it.time.toDate()
		}
	}
	return Pair(change, update)
}