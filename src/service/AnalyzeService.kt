package service

import com.code.analyzer.AnalyzedFile
import com.code.analyzer.Analyzer
import com.code.analyzer.CommitAnalyzer
import com.code.analyzer.dao.models.History
import com.code.analyzer.git.GSON
import com.code.analyzer.git.updateProject
import com.code.analyzer.utils.sortByValueDesc
import com.google.common.collect.Lists
import com.google.common.collect.Maps
import dao.dao
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.locations.post
import io.ktor.request.receive
import io.ktor.response.respondText
import io.ktor.routing.Route
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.eclipse.jgit.api.Git
import org.joda.time.DateTime
import service.json.JsonCollection
import service.models.Action.ANALYZED
import service.models.Action.FAILED_ANALYZED
import service.models.Action.START_ANALYZED
import service.models.Status
import java.io.BufferedWriter
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStreamWriter
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.regex.Pattern

private const val GIT_EXTENSION = ".git"
private const val SLASH = "/"

private val jacocoPattern = Pattern.compile("^.*\\.exec$")
private val pattern = Pattern.compile("^\\..*$")

@KtorExperimentalLocationsAPI
@Location("/{projectId}/analyze")
data class ProjectAnalyze(val projectId: Long = 0)

@KtorExperimentalLocationsAPI
fun Route.analyze() {
	post<ProjectAnalyze> {
		val request = call.receive<ProjectAnalyze>()
		val project = dao!!.findProject(request.projectId)
		if (project == null) {
			call.respondText { "No project" }
			return@post
		}
		var date = DateTime(System.currentTimeMillis())
		updateProject(project.owner, project.name, project.id, date)
		dao!!.createHistory(History(0, project.id, date, START_ANALYZED.id))
		try {
			val projectGit = withContext(Dispatchers.IO) { Git.open(File(project.owner.toString() + SLASH + project.name + SLASH + GIT_EXTENSION)) }
			val (revCommit, revBugs) = CommitAnalyzer(projectGit, Maps.newHashMap(), Maps.newHashMap()).analyze()
			val projectDir = File(project.owner.toString() + SLASH + project.name)
			if (projectDir.exists() && projectDir.isDirectory) {
				val (classFiles, jacocoFiles) = searchAllClassFiles(projectDir, Lists.newArrayList(), Lists.newArrayList())
				val analyzedFiles = arrayListOf<AnalyzedFile>()
				jacocoFiles.forEach {
					Analyzer(analyzedFiles, classFiles, date).execute(it)
				}
				val files = arrayListOf<FileInfo>()
				revCommit.forEach {
					if (!getFileFromList(it.key.split("/").last().split(".").first(), files)) {
						val commits = it.value
						val bugs = revBugs[it.key]?.size ?: 0
						val analyzedFile = getAnalyzedFile(it.key.split("/").last().split(".").first(), analyzedFiles)
						if (analyzedFile != null) {
							val protection = getProtection(commits, bugs, analyzedFile.coverage!!) / 2
							files.add(
								FileInfo(
									analyzedFile.fileName,
									analyzedFile.filePath,
									analyzedFile.coverage,
									analyzedFile.lines,
									analyzedFile.linesCount,
									analyzedFile.time,
									commits,
									bugs,
									protection
								)
							)
						}
					}
				}
				files.sortBy { it.protection }
				val (topEdit, topBug) = getTopAndBug(files)
				files.forEach {
					dao!!.createFile(com.code.analyzer.dao.models.File(0, it.name, project.id, it.linesCount!!, it.commits, it.bugs, it.time, it.coverage!!, topEdit[it.name] ?: error(""),
						topBug[it.name] ?: error("")
					))
				}
				val array = putFileLinesToGson(files)
				val bw = BufferedWriter(OutputStreamWriter(FileOutputStream(projectDir.absolutePath + SLASH + "files.txt"), StandardCharsets.UTF_8))
				bw.append(GSON.toJson(JsonCollection(array), JsonCollection::class.java))
				bw.close()
				dao!!.createHistory(History(0, project.id, date, ANALYZED.id))
				project.status = Status.PASSED.id
				val (bugs, lines) = getCountBugsAndLines(files)
				project.lines = lines
				project.bugs = bugs
				if (project.newCoverage != null) {
					project.coverage = project.coverage
					project.newCoverage = getCoverage(files)
				} else {
					project.coverage = getCoverage(files)
				}
				project.classChangePercent = (100.toDouble() / revCommit.size.toDouble() * revBugs.size.toDouble()).toInt()
				dao!!.updateProject(project)
				call.respondText { "Analyze completed" }
			} else {
				date = DateTime(System.currentTimeMillis())
				dao!!.createHistory(History(0, project.id, date, FAILED_ANALYZED.id))
				project.status = Status.FAILED.id
				dao!!.updateProject(project)
			}
		} catch (e: Exception) {
			date = DateTime(System.currentTimeMillis())
			dao!!.createHistory(History(0, project.id, date, FAILED_ANALYZED.id))
			project.status = Status.FAILED.id
			dao!!.updateProject(project)
			println(e.message)
		}
	}
}

fun searchAllClassFiles(dir: File?, list: ArrayList<ClassFile>, jacocoFiles: ArrayList<String>): Pair<ArrayList<ClassFile>, ArrayList<String>> {
	if (dir?.listFiles() != null && dir.isDirectory) {
		Arrays.stream(Objects.requireNonNull(dir.listFiles())).forEach { searchAllClassFiles(it, list, jacocoFiles) }
	} else {
		if (jacocoPattern.matcher(Objects.requireNonNull(dir)!!.name).find()) {
			jacocoFiles.add(dir!!.absolutePath);
		} else if (dir!!.absolutePath.contains("build") && !dir.absolutePath.contains("test") && dir.absolutePath.contains("classes") && dir.extension == "class") {
			list.add(ClassFile(dir.name, dir.absolutePath, dir.absolutePath.split("main\\")[1]))
		}
	}
	return Pair(list, jacocoFiles)
}

fun getAnalyzedFile(fileName: String, list: List<AnalyzedFile>): AnalyzedFile? {
	list.forEach {
		if (it.fileName == fileName || it.fileName == fileName + "Kt") {
			return it
		}
	}
	return null
}

fun getFileFromList(fileName: String, list: List<FileInfo>): Boolean {
	list.forEach {
		if (it.name == fileName || it.name == fileName + "Kt") {
			return true
		}
	}
	return false
}

fun getCountBugsAndLines(list: List<FileInfo>): Pair<Int, Int> {
	var count = 0
	var lines = 0
	list.forEach {
		if (it.bugs > 0 && it.coverage!! < 80.toDouble()) {
			count++
		}
		lines += it.linesCount!!
	}
	return Pair(count, lines)
}

fun getCoverage(list: List<FileInfo>): Double {
	var coverage = 0.0
	list.forEach {
		coverage += it.coverage!!
	}
	return coverage / list.size
}

fun putFileLinesToGson(files: List<FileInfo>): ArrayList<JsonCollection.File> {
	val array = arrayListOf<JsonCollection.File>()
	files.forEach {
		array.add(JsonCollection.File(it.name, it.lines!!))
	}
	return array
}

fun getTopAndBug(files: List<FileInfo>): Pair<Map<String, Int>, Map<String, Int>> {
	val topEdit = Maps.newHashMap<String, Int>()
	val topProtect = Maps.newHashMap<String, Int>()
	for (i in files.indices) {
		topProtect[files[i].name] = i
	}
	files.sortedBy { it.commits }
	for (i in files.indices) {
		topEdit[files[i].name] = i
	}
	sortByValueDesc(topEdit)
	return Pair(topEdit, topProtect)
}

fun getProtection(commits: Int, bugs: Int, coverage: Double): Double {
	return if (bugs == 0) {
		100 + coverage
	} else if (coverage == 100.toDouble()) {
		200.toDouble()
	} else {
		100 - (100 / commits * bugs) + coverage
	}
}

data class ClassFile(
	val name: String,
	val absolutePath: String,
	val jacocoPath: String
)

data class FileInfo(
	val name: String,
	val path: String?,
	val coverage: Double?,
	val lines: MutableMap<Int, String>?,
	val linesCount: Int?,
	val time: DateTime?,
	val commits: Int,
	val bugs: Int,
	val protection: Double
)