package service.json

data class JsonCollection(
	val files : ArrayList<File>
) {
	data class File(
		val fileName : String,
		val lines: Map<Int, String>
	)
}