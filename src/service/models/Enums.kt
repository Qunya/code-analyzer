package service.models

enum class Status(val id: Int, val message: String) {
	DELETE(0, "Delete"),
	CREATED(1, "Created"),
	IN_PROGRESS(2, "In Progress"),
	PASSED(3, "Passed"),
	FAILED(4, "Failed")
}

enum class Action(val id: Int, val message: String) {
	DELETE(0, "Delete"),
	CREATED(1, "Created"),
	UPDATED(2, "Updated"),
	ANALYZED(3, "Analyzed"),
	START_ANALYZED(3, "Start Analyzed"),
	FAILED_ANALYZED(4, "Failed Analyzed")
}

fun getMessage(id: Int) : String {
	Status.values().forEach { if(id == it.id) { return it.message } }
	return "no have"
}

fun getAction(id: Int) : String {
	Action.values().forEach { if(id == it.id) { return it.message } }
	return "no have"
}