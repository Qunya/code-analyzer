package service.responses

import java.util.*
import kotlin.collections.ArrayList

data class ProjectsResponse(
	val projects: ArrayList<Project>
) {
	data class Project(
		val id: String,
		val name: String,
		val status: String
	) {
		constructor(id: String, name: String, status: String, programmingLanguage: String?, coveragePercent: Double?, dateLastChange: Date?, dateLastUpdate: Date?, numberOfBugs: Int?, numberOfLinesOfCode: Int?, classChangesPercent: Int?) : this(id, name, status) {
			this.programmingLanguage = programmingLanguage
			this.coveragePercent = coveragePercent
			this.dateLastChange = dateLastChange
			this.dateLastUpdate = dateLastUpdate
			this.numberOfBugs = numberOfBugs
			this.numberOfLinesOfCode = numberOfLinesOfCode
			this.classChangesPercent = classChangesPercent
		}
		var programmingLanguage: String? = null
		var coveragePercent: Double? = null
		var dateLastChange: Date? = null
		var dateLastUpdate: Date? = null
		var numberOfBugs: Int? = null
		var numberOfLinesOfCode: Int? = null
		var classChangesPercent: Int? = null
	}
}