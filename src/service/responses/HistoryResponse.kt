package service.responses

data class HistoryResponse(
	val history: ArrayList<History>
) {
	data class History(
		val action: String,
		val date: String
	)
}