package com.code.analyzer.git.responses

data class ProjectInfoResponse(
	val branchName: String,
	val version: Double,
	val before: Before?,
	val after: After?
) {
	data class Before(
		val percent: String,
		val topFiles: ArrayList<File>,
		val potentialBugs: ArrayList<Bug>
	)

	data class After(
		val percent: String,
		val topFiles: ArrayList<File>,
		val potentialBugs: ArrayList<Bug>
	)
}

data class File(val name: String, val changes: Int, val bugs: Int)
data class Bug(val name: String, val changes: Int, val bugs: Int, val coverage: Int)