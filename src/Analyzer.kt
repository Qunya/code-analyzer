package com.code.analyzer


import com.google.common.collect.Maps
import org.jacoco.core.analysis.Analyzer
import org.jacoco.core.analysis.CoverageBuilder
import org.jacoco.core.analysis.ICounter
import org.jacoco.core.data.ExecutionDataReader
import org.jacoco.core.data.ExecutionDataStore
import org.joda.time.DateTime
import service.ClassFile
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import java.util.*

class Analyzer
	(private val analyzedFiles : ArrayList<AnalyzedFile>,
	private val classFiles: ArrayList<ClassFile>,
	private val date: DateTime) {

	@Throws(IOException::class)
	fun execute(file: String) {
		val `in` = FileInputStream(file)
		val reader = ExecutionDataReader(`in`)
		reader.setSessionInfoVisitor { _ -> }
		reader.setExecutionDataVisitor { data ->
			if(data.name.split("\$").size == 1 && !contains(analyzedFiles, data.name) && containsInClassFiles(classFiles, data.name)) {
				val analyzedFile = AnalyzedFile(data.name.split("/").last())
				val executionData = ExecutionDataStore()
				executionData.put(data)
				val coverageBuilder = CoverageBuilder()
				val analyzer = Analyzer(executionData, coverageBuilder)
				try {
					val original = getTargetClass(data.name)
					if (original != null) {
						analyzer.analyzeClass(original, data.name)
						original.close()
						for (cc in coverageBuilder.classes) {
							val lines = Maps.newHashMap<Int, String>()
							for (i in cc.firstLine..cc.lastLine) {
								lines[i] = getColor(cc.getLine(i).status)
							}
							analyzedFile.lines = lines
							analyzedFile.linesCount = cc.lastLine
						}
						analyzedFile.filePath = data.name
						analyzedFile.coverage = 100.toDouble() / data.probes.size * getHitCount(data.probes)
						analyzedFile.time = date
						analyzedFiles.add(analyzedFile)
					}
				} catch (e: Exception) {

				}
			}
		}
		reader.read()
		`in`.close()
	}

	private fun getHitCount(data: BooleanArray): Int {
		var count = 0
		for (hit in data) {
			if (hit) {
				count++
			}
		}
		return count
	}

	private fun getTargetClass(filePath: String): InputStream? {
		val path = getFilePath(filePath)
		return when (path == null) {
			false -> FileInputStream(path)
			true -> null
		}
	}

	private fun getColor(status: Int): String {
		when (status) {
			ICounter.NOT_COVERED -> return "red"
			ICounter.PARTLY_COVERED -> return "yellow"
			ICounter.FULLY_COVERED -> return "green"
		}
		return ""
	}

	private fun getFilePath(filePath: String) : String? {
		classFiles.forEach {
			if(it.name == "${filePath.split("/").last()}.class"){
				return it.absolutePath
			}
		}
		return null
	}

	private fun contains(list: ArrayList<AnalyzedFile>, newFile: String) : Boolean {
		list.forEach {
			if(it.filePath == newFile){
				return true
			}
		}
		return false
	}

	private fun containsInClassFiles(list: ArrayList<ClassFile>, fileName: String) : Boolean {
		list.forEach {
			if(it.name == "${fileName.split("/").last()}.class"){
				return true
			}
		}
		return false
	}

}

data class AnalyzedFile(val fileName: String) {
	constructor(fileName: String, filePath: String, coverage: Double, lines: MutableMap<Int, String>, linesCount: Int, time: DateTime) : this(fileName) {
		this.filePath = filePath
		this.coverage = coverage
		this.linesCount = linesCount
		this.time = time
		this.lines = lines
	}
	var filePath: String? = null
	var coverage: Double? = null
	var lines: MutableMap<Int, String>? = null
	var linesCount: Int? = null
	var time: DateTime? = null
}